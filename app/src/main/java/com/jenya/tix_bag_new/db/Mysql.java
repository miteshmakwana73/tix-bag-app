package com.jenya.tix_bag_new.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.jenya.tix_bag_new.model.favourite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mitesh on 19/4/18.
 */

public class Mysql extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "TixBagDB";
    private static final int DATABASE_VERSION = 1;
    private Context mycontext;

    public Mysql(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mycontext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table tbl_Favouurite(id INTEGER PRIMARY KEY AUTOINCREMENT ,name TEXT,parent_category_id TEXT,image TEXT)");  //
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table if exists tbl_Favouurite");
        this.onCreate(db);
    }

    public void insertIntoDB(int id, String name, String parent_category_id, String image) {
        //Log.d("insert", "before insert");
        int status;
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("name", name);
        values.put("parent_category_id", parent_category_id);
        values.put("image", image);

        // 3. insert
        long rowInserted = db.insert("tbl_Favouurite", null, values);
        if (rowInserted != -1) {
            /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
            Toast.makeText(mycontext, "Added to favourites", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(mycontext, "Something wrong", Toast.LENGTH_SHORT).show();
            //deleteARow(String.valueOf(id));

        }

        // 4. close
        db.close();
        Toast.makeText(mycontext, "insert value", Toast.LENGTH_LONG);
//        Log.i("insert into DB", "After insert");

    }

    /* Retrive  data from database */
    public List<favourite> getDataFromDB() {
        List<favourite> modelList = new ArrayList<favourite>();
        String query = "select * from tbl_Favouurite";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {

                favourite model = new favourite();
                model.setId(cursor.getInt(cursor.getColumnIndex("id")));
                model.setPerformer_name(cursor.getString(cursor.getColumnIndex("name")));
                model.setParent_category_id(cursor.getString(cursor.getColumnIndex("parent_category_id")));
                model.setPerformer_image(cursor.getString(cursor.getColumnIndex("image")));

//                Log.e("id "+cursor.getInt(cursor.getColumnIndex("id"))+" quote "+cursor.getString(cursor.getColumnIndex("name"))
//                ," By "+cursor.getString(cursor.getColumnIndex("image")));

                modelList.add(model);
            } while (cursor.moveToNext());
        }

        //Log.d("Quote data", modelList.toString());

        return modelList;
    }

    /*delete a row from database*/

    public void deleteARow(String quoteid) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("tbl_Favouurite", "id" + " = ?", new String[]{quoteid});
        Toast.makeText(mycontext, "Removed from favourites", Toast.LENGTH_SHORT).show();
        db.close();
    }

    public Cursor getAllFavourites() {
        SQLiteDatabase db = getReadableDatabase();
        String SelectQuery = "Select * from tbl_Favouurite";
        Cursor cursor = db.rawQuery(SelectQuery, null);

        return cursor;
    }

}

