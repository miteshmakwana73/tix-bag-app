package com.jenya.tix_bag_new.activity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.Image;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jenya.tix_bag_new.R;
import com.jenya.tix_bag_new.adapter.FeaturedPerformanceAdapter;
import com.jenya.tix_bag_new.adapter.SearchEventAdapter;
import com.jenya.tix_bag_new.jsonurl.Config;
import com.jenya.tix_bag_new.model.featuredperformer;
import com.jenya.tix_bag_new.model.searchevent;
import com.jenya.tix_bag_new.util.AnalyticsApplication;
import com.jenya.tix_bag_new.util.BottomNavigationViewHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.saeid.fabloading.LoadingView;

/**
 * Created by Mitesh Makwana on 08/05/18.
 */
public class SearchActivity extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener{
    private static final int ACTIVITY_NUM = 1;
    private Context mContext = SearchActivity.this;

    LinearLayout lnview;
    EditText search;
    TextView tvperformers,tvtopresult;//startdate,enddate
//    LinearLayout lnstartdate,lnenddate;
    TextView today,tomorrow,weekend,tvdate;
    LinearLayout lntoday,lntomorrow,lnweekend;
    RelativeLayout lnmain;
    private RecyclerView recyclerView;
    private SearchEventAdapter adapter;
    private List<searchevent> albumList;
    private List<featuredperformer> performerList;
    FeaturedPerformanceAdapter performerAdapter;
    RecyclerView performerRecyclerView;
    ProgressBar progressBar;
    private LoadingView mLoadingView;
    LinearLayout loading;

    SwipeRefreshLayout sw_refresh;

    TextView status;
    RelativeLayout rlerror;
    ImageView imgerror;

    String HttpUrl = Config.SEARCHEVENT;
    Calendar myCalendar = Calendar.getInstance();
    int count=0;

    String start="Start Date",end="End Date";
    boolean doubleBackToExitPressedOnce = false;

    boolean allowsearch = false;
    String weekendstart="",weekendend="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setupBottomNavigationView();

        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        initfab();

        search=(EditText)findViewById(R.id.editsearch);
//        startdate=(TextView) findViewById(R.id.tvstartdate);
//        enddate=(TextView) findViewById(R.id.tvenddate);
//        lnstartdate=(LinearLayout) findViewById(R.id.lnstartdate);
//        lnenddate=(LinearLayout) findViewById(R.id.lnenddate);
        tvperformers=(TextView) findViewById(R.id.tvperformers);
        tvtopresult=(TextView) findViewById(R.id.tvtopresult);
        lnview=(LinearLayout) findViewById(R.id.lnview);
        lnmain=(RelativeLayout) findViewById(R.id.lndata);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        status=(TextView)findViewById(R.id.tvstatus);
        imgerror=(ImageView) findViewById(R.id.imgerror);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);
        loading=(LinearLayout)findViewById(R.id.lnloading);


        today=(TextView)findViewById(R.id.tvtoday);
        tomorrow=(TextView)findViewById(R.id.tvtomorrow);
        weekend=(TextView)findViewById(R.id.tvweekend);
        tvdate=(TextView)findViewById(R.id.tvdate);

        lntoday=(LinearLayout)findViewById(R.id.lntoday);
        lntomorrow=(LinearLayout)findViewById(R.id.lntomorrow);
        lnweekend=(LinearLayout)findViewById(R.id.lnweekend);

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
//        startdate.setTypeface(tf);
//        enddate.setTypeface(tf);
        search.setTypeface(tf);
        status.setTypeface(tf);
        today.setTypeface(tf);
        tomorrow.setTypeface(tf);
        weekend.setTypeface(tf);
        tvdate.setTypeface(tf);
        tvperformers.setTypeface(tf);
        tvtopresult.setTypeface(tf);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        albumList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });


        lntoday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(count==1)
                {
                    count=0;
                    tvdate.setText(getResources().getString(R.string.customDate));
                    weekendstart="";
                    weekendend="";
                    today.setTextColor(getResources().getColor(R.color.black));
                    lntoday.setBackgroundResource(0);

                }
                else
                {
                    count=1;
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
                    today.setTextColor(getResources().getColor(R.color.colorPrimary));
                    tomorrow.setTextColor(getResources().getColor(R.color.black));
                    weekend.setTextColor(getResources().getColor(R.color.black));
                    lntoday.setBackgroundResource(R.drawable.searchfiltereffect);
                    lntomorrow.setBackgroundResource(0);
                    lnweekend.setBackgroundResource(0);
                    Date date = null;
                    try {
                        String dat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                        date = new SimpleDateFormat("yyyy-MM-dd").parse(String.valueOf(dat));
                        String dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", date); // Thursday
                        String day          = (String) android.text.format.DateFormat.format("dd",   date); // 20
                        String monthString  = (String) android.text.format.DateFormat.format("MMM",  date); // Jun
                        String monthNumber  = (String) android.text.format.DateFormat.format("MM",   date); // 06
                        String year         = (String) android.text.format.DateFormat.format("yyyy", date); // 2

                        tvdate.setText(monthString+" "+day);

                        weekendstart=year+"/"+monthNumber+"/"+day;
                        weekendend=year+"/"+monthNumber+"/"+day;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        lntomorrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(count==2)
                {
                    count=0;
                    tvdate.setText(getResources().getString(R.string.customDate));
                    weekendstart="";
                    weekendend="";
                    tomorrow.setTextColor(getResources().getColor(R.color.black));
                    lntomorrow.setBackgroundResource(0);

                }
                else
                {
                    count=2;
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
                    today.setTextColor(getResources().getColor(R.color.black));
                    tomorrow.setTextColor(getResources().getColor(R.color.colorPrimary));
                    weekend.setTextColor(getResources().getColor(R.color.black));
                    lntomorrow.setBackgroundResource(R.drawable.searchfiltereffect);
                    lntoday.setBackgroundResource(0);
                    lnweekend.setBackgroundResource(0);

                    Date date = null;
                    try {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        Date tomorrow = calendar.getTime();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        String tomorrowAsString = dateFormat.format(tomorrow);
                        date = new SimpleDateFormat("yyyy-MM-dd").parse(String.valueOf(tomorrowAsString));
                        String dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", date); // Thursday
                        String day          = (String) android.text.format.DateFormat.format("dd",   date); // 20
                        String monthString  = (String) android.text.format.DateFormat.format("MMM",  date); // Jun
                        String monthNumber  = (String) android.text.format.DateFormat.format("MM",   date); // 06
                        String year         = (String) android.text.format.DateFormat.format("yyyy", date); // 2

                        tvdate.setText(monthString+" "+day);

                        weekendstart=year+"/"+monthNumber+"/"+day;
                        weekendend=year+"/"+monthNumber+"/"+day;

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        lnweekend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(count==3)
                {
                    count=0;
                    tvdate.setText(getResources().getString(R.string.customDate));
                    weekendstart="";
                    weekendend="";
                    weekend.setTextColor(getResources().getColor(R.color.black));
                    lnweekend.setBackgroundResource(0);

                }
                else
                {
                    count=3;
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
                    today.setTextColor(getResources().getColor(R.color.black));
                    tomorrow.setTextColor(getResources().getColor(R.color.black));
                    weekend.setTextColor(getResources().getColor(R.color.colorPrimary));
                    lnweekend.setBackgroundResource(R.drawable.searchfiltereffect);
                    lntoday.setBackgroundResource(0);
                    lntomorrow.setBackgroundResource(0);

                    Calendar cal = Calendar.getInstance(Locale.UK);
                    cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM");

                    String tempstartdate = "",tempenddate="";
                    for (int i = 0; i < 7; i++) {
                        Date day=cal.getTime();
                        Log.i("dateTag", sdf.format(cal.getTime()));
                        if(day.toString().contains("Fri")||day.toString().contains("Sat")||day.toString().contains("Sun"))
                        {
                            Log.e("dateTag", sdf.format(cal.getTime()));
                        }

                        if(day.toString().contains(("Fri")))
                        {
                            String myFormat = "yyyy/MM/dd";
                            SimpleDateFormat sdfapi = new SimpleDateFormat(myFormat, Locale.US);
                            weekendstart=sdfapi.format(cal.getTime());
                            tempstartdate=sdf.format(cal.getTime());
                        }
                        if(day.toString().contains(("Sun")))
                        {
                            String myFormat = "yyyy/MM/dd";
                            SimpleDateFormat sdfapi = new SimpleDateFormat(myFormat, Locale.US);
                            weekendend=sdfapi.format(cal.getTime());
                            tempenddate=sdf.format(cal.getTime());
                        }
                        cal.add(Calendar.DAY_OF_WEEK, 1);
                    }

                    tvdate.setText(tempstartdate+" - "+tempenddate);
                }


            }
        });

        tvdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showDialog();
//                Calendar now = Calendar.getInstance();
                com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        SearchActivity.this,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAutoHighlight(true);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
        /*final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.setTimeInMillis(System.currentTimeMillis() - 1000);

                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        lnstartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

                count=1;
            }
        });

        lnenddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

                count=2;
            }
        });*/

        search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (search.getRight() - search.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(search.getWindowToken(), 0);

                        if (TextUtils.isEmpty(search.getText().toString().trim())) {
                            search.setError("Enter search");
                            return false;
                        }
                        allowsearch=true;
                        checkconnection();

                        return true;
                    }
                }
                return false;
            }
        });

        search.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
                    // password = inputPassword.getText().toString().trim();

                    if (TextUtils.isEmpty(search.getText().toString().trim())) {
                        search.setError("Enter search");
                        return false;
                    }
                    allowsearch=true;
                    checkconnection();
                    return true;
                }
                return false;
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(search.getText().toString().length()>3)
                        {
                            allowsearch=true;
                            albumList.clear();
                            performerList.clear();
                            checkconnection();
                        }
                        else
                        {
                            allowsearch=false;
                        }

                    }
                }, 2000);            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        albumList = new ArrayList<>();
        performerList = new ArrayList<>();
        adapter = new SearchEventAdapter(mContext,albumList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        performerAdapter = new FeaturedPerformanceAdapter(mContext,performerList,3);
        performerRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_performar);
        RecyclerView.LayoutManager mLayoutPManager = new GridLayoutManager(mContext, 1);
        performerRecyclerView.setLayoutManager(mLayoutPManager);
        performerRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        performerRecyclerView.setItemAnimator(new DefaultItemAnimator());
        performerRecyclerView.setAdapter(performerAdapter);
        performerRecyclerView.setNestedScrollingEnabled(false);

        recyclerView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(v);
                return false;
            }
        });

        performerRecyclerView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                hideKeyboard(v);

                return false;
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
        loading.setVisibility(View.GONE);
            }
        }, 100);
//        loading.setVisibility(View.GONE);

    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth,int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String stdate = "You picked the following date: From- "+dayOfMonth+"/"+(++monthOfYear)+"/"+year+" To "+dayOfMonthEnd+"/"+(++monthOfYearEnd)+"/"+yearEnd;
//        int monthstart=monthOfYear+1,monthend=monthOfYearEnd+1;
        weekendstart=year+"-"+monthOfYear+"-"+dayOfMonth;
        weekendend=yearEnd+"-"+monthOfYearEnd+"-"+dayOfMonthEnd;

        String startdate=weekendstart;
        String enddate=weekendend,startday,endday;
        Date datestart,dateend;
        try {
            datestart= new SimpleDateFormat("yyyy-MM-dd").parse(weekendstart);
            dateend= new SimpleDateFormat("yyyy-MM-dd").parse(weekendend);

            String startmonthString  = (String) android.text.format.DateFormat.format("MMM",  datestart); // Jun
            String endmonthString  = (String) android.text.format.DateFormat.format("MMM",  dateend); // Jun
            startday = (String) android.text.format.DateFormat.format("dd",   datestart); // 20
            endday = (String) android.text.format.DateFormat.format("dd",   dateend); // 20

            tvdate.setText(startmonthString+" "+startday+" - "+endmonthString+" "+endday);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    private void initfab() {
        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_2 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_3 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_4 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });
        mLoadingView.startAnimation();
    }

/*
    private void updateLabel() {
//        String myFormat = "MM/dd/yy"; //In which you need put here
        String myFormat = "yyyy/MM/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if(count==1)
        {
            startdate.setText(sdf.format(myCalendar.getTime()));
        }
        else
        {
            enddate.setText(sdf.format(myCalendar.getTime()));
        }

    }
*/

    private void checkconnection() {
        albumList.clear();
        performerList.clear();
        lnview.setVisibility(View.GONE);
        tvperformers.setVisibility(View.GONE);
        tvtopresult.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
        performerAdapter.notifyDataSetChanged();
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(allowsearch) {
                rlerror.setVisibility(View.GONE);
                tvperformers.setVisibility(View.GONE);
                tvtopresult.setVisibility(View.GONE);

                loadsearchresult();
                allowsearch=false;
            }

        }else {
            //no connection
            lnmain.setVisibility(View.GONE);
            rlerror.setVisibility(View.VISIBLE);
            status.setText(R.string.noconnection);

            showToast("No Connection Found");
        }
    }

    private void loadsearchresult(){

        final String searchh=search.getText().toString().trim();
//        start=startdate.getText().toString().trim();
//        end=enddate.getText().toString().trim();

        if(weekendstart.equals("")||weekendend.equals(""))
        {
            start="Start Date";
            end="End Date";
        }
        else
        {
            start=weekendstart;
            end=weekendend;
        }

        if(TextUtils.isEmpty(searchh) ){
            search.setError("Please enter Search");
            //Toast.makeText(this,"Please enter email",Toast.LENGTH_LONG).show();
            return;
        }

        if(start.equals("Start Date")){
//            showToast("Please select start date");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String date=sdf.format(new Date());
            Log.e("date",date);
            start=date;

//            return;
        }

        if(end.equals("End Date")){
//            showToast("Please select end date");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            Date today = cal.getTime();
            cal.add(Calendar.YEAR, 1); // to get previous year add -1
            Date nextYear = cal.getTime();
            String after1year=sdf.format(nextYear);
            Log.e("date", String.valueOf(nextYear));
            end=after1year;

//            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

        albumList.clear();
        performerList.clear();
        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        mLoadingView.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);

                        try {
//                            Log.e("responce",ServerResponse);
                            albumList.clear();
                            performerList.clear();
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {

                                JSONArray heroArray = null;
                                if(jobj.isNull("events"))
                                {
                                    getPerformerlist(jobj);
                                }
                                else
                                {
                                    getPerformerlist(jobj);

                                    heroArray = jobj.getJSONArray("events");
                                }
                                //now looping through all the elements of the json array
                                for (int i = 0; i < heroArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject heroObject = heroArray.getJSONObject(i);

                                    //creating a hero object and giving them the values from json object
                                    searchevent hero = new searchevent(
                                            heroObject.getInt("ID"),
                                            heroObject.getString("ChildCategoryID"),
                                            heroObject.getString("City"),
                                            heroObject.getString("Clicks"),
                                            heroObject.getString("Date"),
                                            heroObject.getString("DisplayDate"),
                                            heroObject.getString("GrandchildCategoryID"),
                                            heroObject.getString("IsWomensEvent"),
                                            heroObject.getString("MapURL"),
                                            heroObject.getString("InteractiveMapURL"),
                                            heroObject.getString("Name"),
                                            heroObject.getString("ParentCategoryID"),
                                            heroObject.getString("StateProvince"),
                                            heroObject.getString("StateProvinceID"),
                                            heroObject.getString("Venue"),
                                            heroObject.getString("VenueConfigurationID"),
                                            heroObject.getString("VenueID"),
                                            heroObject.getString("CountryID"));

                                    //adding the hero to herolist
                                    albumList.add(hero);
                                }
                                //Log.e("list", String.valueOf(albumList));
                                //creating custom adapter object
                                adapter = new SearchEventAdapter(mContext,albumList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);
                                tvtopresult.setVisibility(View.VISIBLE);
                                lnview.setVisibility(View.VISIBLE);


                            } else {
                                showToast(msg);

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                            getsinglevalue(start,end);

                        }
                        // Showing response message coming from server.
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        mLoadingView.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("event_name", String.valueOf(searchh));
                params.put("start_date", start);
                params.put("end_date", end);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);

    }

    private void getPerformerlist(JSONObject jobj) {
        performerList.clear();
        JSONArray performarArray = null;
        try {
            performarArray = jobj.getJSONArray("performer");
            //now looping through all the elements of the json array
            for (int i = 0; i < performarArray.length(); i++) {
                //getting the json object of the particular index inside the array
                JSONObject performarObject = performarArray.getJSONObject(i);

                //creating a hero object and giving them the values from json object
                featuredperformer performar = new featuredperformer(
                        performarObject.getString("performer_id"),
                        performarObject.getString("parent_category_id"),
                        performarObject.getString("image"),
                        performarObject.getString("name"));

                //adding the hero to herolist
                performerList.add(performar);
            }
            performerAdapter = new FeaturedPerformanceAdapter(mContext,performerList,3);

            performerRecyclerView.setAdapter(performerAdapter);

            tvperformers.setVisibility(View.VISIBLE);
//            return;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getsinglevalue(final String start, final String end){

        final String searchh=search.getText().toString().trim();
//        final String start=startdate.getText().toString().trim();
//        final String end=enddate.getText().toString().trim();

        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        mLoadingView.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {

                                JSONObject heroArray = null;

                                if(jobj.isNull("events"))
                                {
                                    getPerformerlist(jobj);
                                }
                                else
                                {
                                    getPerformerlist(jobj);

                                    heroArray = jobj.getJSONObject("events");
                                }
                                searchevent hero = new searchevent(
                                        heroArray.getInt("ID"),
                                        heroArray.getString("ChildCategoryID"),
                                        heroArray.getString("City"),
                                        heroArray.getString("Clicks"),
                                        heroArray.getString("Date"),
                                        heroArray.getString("DisplayDate"),
                                        heroArray.getString("GrandchildCategoryID"),
                                        heroArray.getString("IsWomensEvent"),
                                        heroArray.getString("MapURL"),
                                        heroArray.getString("InteractiveMapURL"),
                                        heroArray.getString("Name"),
                                        heroArray.getString("ParentCategoryID"),
                                        heroArray.getString("StateProvince"),
                                        heroArray.getString("StateProvinceID"),
                                        heroArray.getString("Venue"),
                                        heroArray.getString("VenueConfigurationID"),
                                        heroArray.getString("VenueID"),
                                        heroArray.getString("CountryID"));

                                albumList.add(hero);

                                //Log.e("list", String.valueOf(albumList));
                                //creating custom adapter object
                                adapter = new SearchEventAdapter(mContext,albumList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);
                                tvtopresult.setVisibility(View.VISIBLE);
                                lnview.setVisibility(View.VISIBLE);
                            } else {
                                showToast(msg);
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            if(albumList.size()==0 && performerList.size()==0) {
                                hideKeyboard(search);
                                rlerror.setVisibility(View.VISIBLE);
                                status.setText(R.string.nodata);
                                imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            }
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        mLoadingView.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("event_name", searchh);
                params.put("start_date", start);
                params.put("end_date", end);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);

    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        AnalyticsApplication.getInstance().trackScreenView("Search Activity");

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void setupBottomNavigationView(){
        Log.d("bottom", "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext, this,bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);
    }
}
