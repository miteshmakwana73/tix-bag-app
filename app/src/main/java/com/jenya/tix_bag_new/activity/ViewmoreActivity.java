package com.jenya.tix_bag_new.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jenya.tix_bag_new.R;
import com.jenya.tix_bag_new.adapter.CategoryAdapter;
import com.jenya.tix_bag_new.adapter.FavouriteAdapter;
import com.jenya.tix_bag_new.adapter.FeaturedPerformanceAdapter;
import com.jenya.tix_bag_new.jsonurl.Config;
import com.jenya.tix_bag_new.model.category;
import com.jenya.tix_bag_new.model.featuredperformer;
import com.jenya.tix_bag_new.util.AnalyticsApplication;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.saeid.fabloading.LoadingView;

public class ViewmoreActivity extends AppCompatActivity {

    private Context mContext=ViewmoreActivity.this;

    private RecyclerView recyclerView;
    private FeaturedPerformanceAdapter adapter;
    private List<featuredperformer> albumList;
    RecyclerView.LayoutManager mLayoutManager;
    private List<String> stringList;

    ProgressBar progressBar;
    SwipeRefreshLayout sw_refresh;
    private LoadingView mLoadingView;
    LinearLayout loading;

    LinearLayout lnerror;
    TextView status,tverror;
    ImageView errorimage;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewmore);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            title="";
        } else {
            title= String.valueOf(extras.getString("title"));
            Log.e("title", title);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
//        getActionBar().setTitle(title);
        getSupportActionBar().setTitle(title);

        albumList = new ArrayList<>();
        stringList = new ArrayList<>();

        SharedPreferences mSharedPreference1 =   PreferenceManager.getDefaultSharedPreferences(mContext);
        stringList.clear();
        int size = mSharedPreference1.getInt("Status_size", 0);

        for(int i=0;i<size;i++)
        {
            stringList.add(mSharedPreference1.getString("Status_" + i, null));
        }

        for(int i=0;i<stringList.size();i++)
        {
            Log.e("item",stringList.get(i));
            String[] array = stringList.get(i).split(",");
            try {
                featuredperformer a=new featuredperformer(Integer.parseInt(array[0]),array[1],array[2],array[3]);
                albumList.add(a);
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                Log.e("Error",e.getMessage().toString());
            }

        }

        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        initfab();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        status=(TextView)findViewById(R.id.tvstatus);
        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);
        loading=(LinearLayout)findViewById(R.id.lnloading);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
//                        albumList.clear();

                        adapter.notifyDataSetChanged();

                        mLoadingView.setVisibility(View.VISIBLE);
                        loaddata();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        adapter = new FeaturedPerformanceAdapter(mContext,albumList,2);
        mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        mLoadingView.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        switch (getResources().getConfiguration().orientation) {
            case 1:
                mLayoutManager = new GridLayoutManager(mContext, 1);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
            case 2:
                mLayoutManager = new GridLayoutManager(mContext, 2);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
        }
        AnalyticsApplication.getInstance().trackScreenView(title+" Viewmore Activity");

    }

    private void initfab() {
        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_2 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_3 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_4 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });
        mLoadingView.startAnimation();

    }

    private void loaddata() {
        mLoadingView.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        adapter = new FeaturedPerformanceAdapter(mContext,albumList,2);
        recyclerView.setAdapter(adapter);
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                // ProjectsActivity is my 'home' activity
                finish();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
