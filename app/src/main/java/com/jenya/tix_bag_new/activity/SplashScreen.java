package com.jenya.tix_bag_new.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jenya.tix_bag_new.R;
import com.jenya.tix_bag_new.util.AnalyticsApplication;

public class SplashScreen extends AppCompatActivity {

    Intent intent;
    View mDecorView;
    ImageView imageView;
    LinearLayout linearLayout;
    int a=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        linearLayout=(LinearLayout)findViewById(R.id.lnsplash);
        /* imageView=(ImageView)findViewById(R.id.centerImage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rippleBackground.startRippleAnimation();
            }
        });*/

        //changecolor(a);
        mDecorView = getWindow().getDecorView();

        hideSystemUI();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                        intent = new Intent(SplashScreen.this,WelcomeActivity.class);
                        startActivity(intent);
                        finish();
                } catch (Exception e) {

                }
            }
        }, 3000);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AnalyticsApplication.getInstance().trackScreenView("Splash Screen");

    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
}
