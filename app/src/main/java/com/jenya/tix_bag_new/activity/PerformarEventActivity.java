package com.jenya.tix_bag_new.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.jenya.tix_bag_new.R;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.tix_bag_new.adapter.PerformarEventAdapter;
import com.jenya.tix_bag_new.jsonurl.Config;
import com.jenya.tix_bag_new.model.performerevent;
import com.jenya.tix_bag_new.util.AnalyticsApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.saeid.fabloading.LoadingView;

/**
 * Created by Mitesh Makwana on 08/05/18.
 */
public class PerformarEventActivity extends AppCompatActivity {
    private Context mContext = PerformarEventActivity.this;

    private RecyclerView recyclerView;
    private PerformarEventAdapter adapter;
    private List<performerevent> albumList;
    ProgressBar progressBar;
    private LoadingView mLoadingView;
    LinearLayout loading;

    SwipeRefreshLayout sw_refresh;

    ImageView performarimage;
    RelativeLayout rlerror;
    TextView status,tverror;
    ImageView errorimage;

    String HttpUrl = Config.PERFORMERSEVENT;

    private int performer_id;
    private String performer_name;
    private String parent_category_id;
    private String performer_image;
    Toolbar toolbar;
    Typeface tf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performar);

        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        initfab();

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        performer_id = sharedPreferences.getInt("performer_id",0);
        performer_name = sharedPreferences.getString("name","");
        parent_category_id = sharedPreferences.getString("parent_category_id","");
        performer_image = sharedPreferences.getString("image","");

//        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#000000\">" + performer_name+ "</font>")));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(performer_name);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        status=(TextView)findViewById(R.id.tvstatus);
        errorimage=(ImageView) findViewById(R.id.imgerror);
        tverror=(TextView)findViewById(R.id.tverror);
        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);
        performarimage = (ImageView) findViewById(R.id.imgperformarbg);
        loading=(LinearLayout)findViewById(R.id.lnloading);
        mLoadingView.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);

        tf = Typeface.createFromAsset(getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);

        initCollapsingToolbar();

        /*Glide.with(PerformarEventActivity.this)
                .load(Config.PERFORMERSIMAGE+performer_image)
                .into(performarimage);*/

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        albumList.clear();

                        //Quote a = new Quote("हिंदी", "mitesh2", "mitesh2");
                        //albumList.add(a);
                        //adapter = new QuotesAdapter(getApplicationContext(),albumList);

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });
        albumList = new ArrayList<>();

        /*for(int i=1;i<=5;i++)
        {
            performerevent hero = new performerevent(1,
                    "name"+i,
                    "09:0"+i,
                    "event name"+i);

            //adding the hero to herolist
            albumList.add(hero);
        }*/

        adapter = new PerformarEventAdapter(mContext,albumList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        try {
            Glide.with(mContext).load(Config.PERFORMERSIMAGE+performer_image).into(performarimage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        checkconnection();

    }

    private void initfab() {
        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_2 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_3 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_4 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });
        mLoadingView.startAnimation();

    }

    private void checkconnection() {
//        status.setVisibility(View.GONE);
        rlerror.setVisibility(View.GONE);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {

            rlerror.setVisibility(View.GONE);

            //loadPerformarevent();
            passperformer_id();
            //connection is avlilable
        }else {
            //no connection
            //displayAlert();
            status.setText(R.string.noconnection);
//            status.setVisibility(View.VISIBLE);
            rlerror.setVisibility(View.VISIBLE);
            /*errorimage.setBackgroundResource(R.drawable.charge);
            tverror.setText(R.string.noconnection);*/
//            Toast.makeText(this, "No Connection Found", Toast.LENGTH_SHORT).show();
        }
    }

    private void passperformer_id(){


        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        mLoadingView.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status1.equals("1")) {

                                //move to next page
//                                Toast.makeText(PerformarEventActivity.this, msg, Toast.LENGTH_SHORT).show();

//                                loadPerformarevent();


                                JSONArray heroArray = jobj.getJSONArray("events");

                                //now looping through all the elements of the json array
                                for (int i = 0; i < heroArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject heroObject = heroArray.getJSONObject(i);

                                    //creating a hero object and giving them the values from json object
                                    performerevent hero = new performerevent(
                                            heroObject.getInt("ID"),
                                            heroObject.getString("ChildCategoryID"),
                                            heroObject.getString("City"),
                                            heroObject.getString("Clicks"),
                                            heroObject.getString("Date"),
                                            heroObject.getString("DisplayDate"),
                                            heroObject.getString("GrandchildCategoryID"),
                                            heroObject.getString("IsWomensEvent"),
                                            heroObject.getString("MapURL"),
                                            heroObject.getString("InteractiveMapURL"),
                                            heroObject.getString("Name"),
                                            heroObject.getString("ParentCategoryID"),
                                            heroObject.getString("StateProvince"),
                                            heroObject.getString("StateProvinceID"),
                                            heroObject.getString("Venue"),
                                            heroObject.getString("VenueConfigurationID"),
                                            heroObject.getString("VenueID"),
                                            heroObject.getString("CountryID"));

                                    //adding the hero to herolist
                                    albumList.add(hero);
                                }
                                //Log.e("list", String.valueOf(albumList));
                                //creating custom adapter object
                                adapter = new PerformarEventAdapter(mContext,albumList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);


                            } else {
                                rlerror.setVisibility(View.VISIBLE);
                                errorimage.setBackground(getResources().getDrawable(R.drawable.no_data));
                                status.setText(msg);
//                                status.setVisibility(View.VISIBLE);

                                showToast(msg);

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                            getsinglepassperformer_id();

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        mLoadingView.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();


                // Adding All values to Params.
                params.put("performer_id", String.valueOf(performer_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);

    }

    private void getsinglepassperformer_id(){


        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        mLoadingView.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {

                                //move to next page
//                                Toast.makeText(PerformarEventActivity.this, msg, Toast.LENGTH_SHORT).show();

//                                loadPerformarevent();


                                JSONObject heroArray = jobj.getJSONObject("events");

                                performerevent hero = new performerevent(
                                        heroArray.getInt("ID"),
                                        heroArray.getString("ChildCategoryID"),
                                        heroArray.getString("City"),
                                        heroArray.getString("Clicks"),
                                        heroArray.getString("Date"),
                                        heroArray.getString("DisplayDate"),
                                        heroArray.getString("GrandchildCategoryID"),
                                        heroArray.getString("IsWomensEvent"),
                                        heroArray.getString("MapURL"),
                                        heroArray.getString("InteractiveMapURL"),
                                        heroArray.getString("Name"),
                                        heroArray.getString("ParentCategoryID"),
                                        heroArray.getString("StateProvince"),
                                        heroArray.getString("StateProvinceID"),
                                        heroArray.getString("Venue"),
                                        heroArray.getString("VenueConfigurationID"),
                                        heroArray.getString("VenueID"),
                                        heroArray.getString("CountryID"));

                                albumList.add(hero);

                                //Log.e("list", String.valueOf(albumList));
                                //creating custom adapter object
                                adapter = new PerformarEventAdapter(mContext,albumList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);


                            } else {
                                showToast(msg);
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            rlerror.setVisibility(View.VISIBLE);
                            errorimage.setBackground(getResources().getDrawable(R.drawable.no_data));
                            status.setText(R.string.nodata);

//                            status.setText("Events Not Found");
                            status.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        mLoadingView.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();


                // Adding All values to Params.
                params.put("performer_id", String.valueOf(performer_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);

    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//        collapsingToolbar.setTitle(performer_name);
//        collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
//        collapsingToolbar.setCollapsedTitleTypeface(tf);
//

        Typeface font = Typer.set(mContext).getFont(Font.ROBOTO_MEDIUM);
//        collapsingToolbar.setCollapsedTitleTypeface(font);
        collapsingToolbar.setExpandedTitleTypeface(font);
        collapsingToolbar.setTitle(performer_name);

//        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.black));
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);
//        toolbar.setNavigationIcon(null);


        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(performer_name);
                    toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

                    isShow = true;
                } else if (isShow) {
//                    collapsingToolbar.setTitle(" ");

//                    toolbar.setNavigationIcon(null);

                    isShow = false;
                }
            }
        });
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                // ProjectsActivity is my 'home' activity
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("TAG", "Performer screen name: " + performer_name);

        AnalyticsApplication.getInstance().trackScreenView(performer_name+" Activity");
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}