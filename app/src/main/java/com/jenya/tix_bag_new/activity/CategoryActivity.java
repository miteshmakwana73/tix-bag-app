package com.jenya.tix_bag_new.activity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jenya.tix_bag_new.R;
import com.jenya.tix_bag_new.adapter.CategoryAdapter;
import com.jenya.tix_bag_new.jsonurl.Config;
import com.jenya.tix_bag_new.model.category;
import com.jenya.tix_bag_new.util.AnalyticsApplication;
import com.jenya.tix_bag_new.util.BottomNavigationViewHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.saeid.fabloading.LoadingView;

/**
 * Created by Mitesh Makwana on 04/05/18.
 */
public class CategoryActivity extends AppCompatActivity {
    public static int ACTIVITY_NUM=2;
    private Context mContext=CategoryActivity.this;

    private RecyclerView recyclerView;
    private CategoryAdapter adapter;
    private List<category> albumList;
    RecyclerView.LayoutManager mLayoutManager;
    ProgressBar progressBar;
    private LoadingView mLoadingView;
    LinearLayout loading;

    SwipeRefreshLayout sw_refresh;


    RelativeLayout rlerror;
    TextView status,tverror;
    ImageView errorimage;

    String HttpUrl = Config.CATEGORY;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        setupBottomNavigationView();

        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        initfab();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        status=(TextView)findViewById(R.id.tvstatus);
        errorimage=(ImageView) findViewById(R.id.imgerror);
        tverror=(TextView)findViewById(R.id.tverror);
        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);
        loading=(LinearLayout)findViewById(R.id.lnloading);
        mLoadingView.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        albumList.clear();

                        //Quote a = new Quote("हिंदी", "mitesh2", "mitesh2");
                        //albumList.add(a);
                        //adapter = new QuotesAdapter(getApplicationContext(),albumList);

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        albumList = new ArrayList<>();
        adapter = new CategoryAdapter(mContext,albumList);
        mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();

    }

    private void initfab() {
        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_2 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_3 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_4 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });
        mLoadingView.startAnimation();

    }


    @Override
    protected void onResume() {
        super.onResume();
        switch (getResources().getConfiguration().orientation) {
            case 1:
                mLayoutManager = new GridLayoutManager(mContext, 1);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
            case 2:
                mLayoutManager = new GridLayoutManager(mContext, 2);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
        }

        AnalyticsApplication.getInstance().trackScreenView("Category Activity");

    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
//            status.setVisibility(View.GONE);
            rlerror.setVisibility(View.GONE);

            loadcategory();

        }else {
            //no connection
//            status.setVisibility(View.VISIBLE);
            rlerror.setVisibility(View.VISIBLE);
//            errorimage.setBackgroundResource(R.drawable.charge);
            status.setText(R.string.noconnection);

            showToast("No Connection Found");
        }
    }

    private void loadcategory() {
        //getting the progressbar

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

        albumList.clear();

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);
                        mLoadingView.setVisibility(View.INVISIBLE);
                        loading.setVisibility(View.INVISIBLE);

                        try {
//                            Log.e("Data",response);

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            JSONArray heroArray = obj.getJSONArray("categories");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //creating a hero object and giving them the values from json object
                                category hero = new category(
                                        heroObject.getInt("id"),
                                        heroObject.getString("type"),
                                        heroObject.getString("childcategoryid"),
                                        heroObject.getString("ChildCategoryDescription"),
                                        heroObject.getString("GrandchildCategoryID"),
                                        heroObject.getString("GrandchildCategoryDescription"),
                                        heroObject.getString("title"),
                                        heroObject.getString("image"),
                                        heroObject.getString("content"),
                                        heroObject.getString("app_active"),
                                        heroObject.getString("app_order"),
                                        heroObject.getString("status"));

                                //adding the hero to herolist
                                albumList.add(hero);
                            }
                            //Log.e("list", String.valueOf(albumList));
                            //creating custom adapter object
                            adapter = new CategoryAdapter(mContext,albumList);

                            //adding the adapter to listview
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {

                            getsinglevalue();

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Log.e("error", String.valueOf(error));
                        showToast("Connection slow");
                        //+error.toString()
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void getsinglevalue() {
        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

        albumList.clear();
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);
                        mLoadingView.setVisibility(View.INVISIBLE);
                        loading.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            //Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            //JSONArray heroArray = obj.getJSONArray("response");

                            JSONObject heroArray = obj.getJSONObject("categories");

                            //the response JSON Object
                            //and converts them into javascript objects
                            category hero = new category(
                                    heroArray.getInt("id"),
                                    heroArray.getString("type"),
                                    heroArray.getString("childcategoryid"),
                                    heroArray.getString("ChildCategoryDescription"),
                                    heroArray.getString("GrandchildCategoryID"),
                                    heroArray.getString("GrandchildCategoryDescription"),
                                    heroArray.getString("title"),
                                    heroArray.getString("image"),
                                    heroArray.getString("content"),
                                    heroArray.getString("app_active"),
                                    heroArray.getString("app_order"),
                                    heroArray.getString("status"));

                            albumList.add(hero);

                            //creating custom adapter object
                            adapter = new CategoryAdapter(mContext,albumList);

                            //adding the adapter to listview
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            status.setText(R.string.nodata);
                            rlerror.setVisibility(View.VISIBLE);
                            errorimage.setBackground(getResources().getDrawable(R.drawable.no_data));
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs
                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                        checkconnection();
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }


    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    /**
     * BottomNavigationView setup
     */
    private void setupBottomNavigationView(){
        Log.d("bottom", "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext, this,bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);
    }
}
