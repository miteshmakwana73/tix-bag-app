package com.jenya.tix_bag_new.activity;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jenya.tix_bag_new.R;
import com.jenya.tix_bag_new.adapter.FavouriteAdapter;
import com.jenya.tix_bag_new.db.Mysql;
import com.jenya.tix_bag_new.model.favourite;
import com.jenya.tix_bag_new.util.AnalyticsApplication;
import com.jenya.tix_bag_new.util.BottomNavigationViewHelper;

import java.util.ArrayList;
import java.util.List;

import io.saeid.fabloading.LoadingView;

/**
 * Created by Mitesh Makwana on 04/05/18.
 */
public class FavouriteActivity extends AppCompatActivity {

    private static final int ACTIVITY_NUM = 3;

    private Context mContext = FavouriteActivity.this;
    private RecyclerView recyclerView;
    private FavouriteAdapter adapter;
    private List<favourite> favouriteList;
    RecyclerView.LayoutManager mLayoutManager;
    ProgressBar progressBar;
    private LoadingView mLoadingView;
    LinearLayout loading;


    SwipeRefreshLayout sw_refresh;

    Cursor cursor;

    Mysql mySql;

    TextView status;
    RelativeLayout rlerror;
    ImageView imgerror;


    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);

        setupBottomNavigationView();

        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        initfab();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        status=(TextView)findViewById(R.id.tvstatus);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        imgerror=(ImageView)findViewById(R.id.imgerror);
        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);
        loading=(LinearLayout)findViewById(R.id.lnloading);
        mLoadingView.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        favouriteList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        favouriteList = new ArrayList<>();
        adapter = new FavouriteAdapter(mContext,favouriteList);
        mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();

    }

    @Override
    protected void onResume() {
        super.onResume();
        switch (getResources().getConfiguration().orientation) {
            case 1:
                mLayoutManager = new GridLayoutManager(mContext, 1);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
            case 2:
                mLayoutManager = new GridLayoutManager(mContext, 2);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
        }

        AnalyticsApplication.getInstance().trackScreenView("Favourite Activity");

    }

    private void initfab() {
        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_2 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_3 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_4 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });
        mLoadingView.startAnimation();

    }

    private void checkconnection() {
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            UpdateAdaptrer();

        }else {
            //no connection
            UpdateAdaptrer();

//            Toast.makeText(getActivity(), "No Connection Found", Toast.LENGTH_SHORT).show();
        }
    }

    private void UpdateAdaptrer() {
        progressBar.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
//        status.setVisibility(View.GONE);
        favouriteList.clear();
        mySql = new Mysql(mContext);
        favouriteList = mySql.getDataFromDB();
        if(favouriteList.size()==0)
        {
//            status.setTextColor(getResources().getColor(R.color.white));
            status.setVisibility(View.VISIBLE);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
            rlerror.setVisibility(View.VISIBLE);
            status.setText("favourite list is Empty");
            showToast("favourite list is Empty");
        }

        adapter = new FavouriteAdapter(mContext,favouriteList);
        recyclerView.setAdapter(adapter);

    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void setupBottomNavigationView(){
        Log.e("bottom", "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);

        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext,this,bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);

    }
}
