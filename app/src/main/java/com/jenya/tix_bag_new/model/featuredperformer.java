package com.jenya.tix_bag_new.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class featuredperformer {
    private int id;
    private String performer_name;
    private String parent_category_id;
    private String performer_image;

    public featuredperformer(String performer_id, String parent_category_id, String image, String name) {
        this.id = Integer.parseInt(performer_id);
        this.parent_category_id = parent_category_id;
        this.performer_image = image;
        this.performer_name = name;
    }

    public featuredperformer(int id, String performer_name, String parent_category_id, String performer_image) {
        this.id = id;
        this.performer_name = performer_name;
        this.parent_category_id = parent_category_id;
        this.performer_image = performer_image;
    }

    public featuredperformer() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPerformer_name() {
        return performer_name;
    }

    public void setPerformer_name(String performer_name) {
        this.performer_name = performer_name;
    }

    public String getParent_category_id() {
        return parent_category_id;
    }

    public void setParent_category_id(String parent_category_id) {
        this.parent_category_id = parent_category_id;
    }

    public String getPerformer_image() {
        return performer_image;
    }

    public void setPerformer_image(String performer_image) {
        this.performer_image = performer_image;
    }

}
