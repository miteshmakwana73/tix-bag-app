package com.jenya.tix_bag_new.model;

public class subcategory {
    private int ID;
    private String City;
    private String Clicks;
    private String Date;
    private String DisplayDate;
    private String GrandchildCategoryID;
    private String ChildCategoryID;
    private String IsWomensEvent;
    private String MapURL;
    private String InteractiveMapURL;
    private String Name;
    private String ParentCategoryID;
    private String StateProvince;
    private String StateProvinceID;
    private String Venue;
    private String VenueConfigurationID;
    private String VenueID;
    private String CountryID;

    public subcategory(int ID, String city, String clicks, String date, String displayDate, String grandchildCategoryID, String childCategoryID, String isWomensEvent, String mapURL, String interactiveMapURL, String name, String parentCategoryID, String stateProvince, String stateProvinceID, String venue, String venueConfigurationID, String venueID, String countryID) {
        this.ID = ID;
        City = city;
        Clicks = clicks;
        Date = date;
        DisplayDate = displayDate;
        GrandchildCategoryID = grandchildCategoryID;
        ChildCategoryID = childCategoryID;
        IsWomensEvent = isWomensEvent;
        MapURL = mapURL;
        InteractiveMapURL = interactiveMapURL;
        Name = name;
        ParentCategoryID = parentCategoryID;
        StateProvince = stateProvince;
        StateProvinceID = stateProvinceID;
        Venue = venue;
        VenueConfigurationID = venueConfigurationID;
        VenueID = venueID;
        CountryID = countryID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getClicks() {
        return Clicks;
    }

    public void setClicks(String clicks) {
        Clicks = clicks;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getDisplayDate() {
        return DisplayDate;
    }

    public void setDisplayDate(String displayDate) {
        DisplayDate = displayDate;
    }

    public String getGrandchildCategoryID() {
        return GrandchildCategoryID;
    }

    public void setGrandchildCategoryID(String grandchildCategoryID) {
        GrandchildCategoryID = grandchildCategoryID;
    }

    public String getChildCategoryID() {
        return ChildCategoryID;
    }

    public void setChildCategoryID(String childCategoryID) {
        ChildCategoryID = childCategoryID;
    }

    public String getIsWomensEvent() {
        return IsWomensEvent;
    }

    public void setIsWomensEvent(String isWomensEvent) {
        IsWomensEvent = isWomensEvent;
    }

    public String getMapURL() {
        return MapURL;
    }

    public void setMapURL(String mapURL) {
        MapURL = mapURL;
    }

    public String getInteractiveMapURL() {
        return InteractiveMapURL;
    }

    public void setInteractiveMapURL(String interactiveMapURL) {
        InteractiveMapURL = interactiveMapURL;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getParentCategoryID() {
        return ParentCategoryID;
    }

    public void setParentCategoryID(String parentCategoryID) {
        ParentCategoryID = parentCategoryID;
    }

    public String getStateProvince() {
        return StateProvince;
    }

    public void setStateProvince(String stateProvince) {
        StateProvince = stateProvince;
    }

    public String getStateProvinceID() {
        return StateProvinceID;
    }

    public void setStateProvinceID(String stateProvinceID) {
        StateProvinceID = stateProvinceID;
    }

    public String getVenue() {
        return Venue;
    }

    public void setVenue(String venue) {
        Venue = venue;
    }

    public String getVenueConfigurationID() {
        return VenueConfigurationID;
    }

    public void setVenueConfigurationID(String venueConfigurationID) {
        VenueConfigurationID = venueConfigurationID;
    }

    public String getVenueID() {
        return VenueID;
    }

    public void setVenueID(String venueID) {
        VenueID = venueID;
    }

    public String getCountryID() {
        return CountryID;
    }

    public void setCountryID(String countryID) {
        CountryID = countryID;
    }
}
