package com.jenya.tix_bag_new.model;

public class slider {
    private int ID;
    private String title;
    private String image;

    public slider(int ID, String title, String image) {
        this.ID = ID;
        this.title = title;
        this.image = image;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
