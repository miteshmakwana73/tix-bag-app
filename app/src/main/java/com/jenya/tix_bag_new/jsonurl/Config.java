package com.jenya.tix_bag_new.jsonurl;

public class Config {
	//192.168.1.144:9110 local
	//202.131.97.68:8004 server
	public static final String FEATUREDPERFORMER = "http://api.tixbag.com";
	public static final String PERFORMERSEVENT = "http://api.tixbag.com/performer-events";

	public static final String CATEGORY = "http://api.tixbag.com/get-subcategories";
	public static final String SUB_CATEGORY = "http://api.tixbag.com/get-performer-by-cat";

	public static final String PERFORMERSIMAGE = "https://www.tixbag.com/assets/images/performers/";
	public static final String CATEGORYIMAGE = "https://www.tixbag.com/assets/images/categories/";

	public static final String SEARCHEVENT = "http://api.tixbag.com/search-events";

	public static final String WEBVIEW = "http://api.tixbag.com/get-ticket";


}