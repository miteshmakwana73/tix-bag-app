package com.jenya.tix_bag_new.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jenya.tix_bag_new.R;

import com.bumptech.glide.Glide;
import com.jenya.tix_bag_new.activity.SubcategoryActivity;
import com.jenya.tix_bag_new.jsonurl.Config;
import com.jenya.tix_bag_new.model.category;

import java.util.List;

/**
 * Created by Mitesh Makwana on 08/05/18.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<category> albumList;

    Typeface tf;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        ImageView image;
        CardView cdview;

        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.txtcategoryname);

            image=(ImageView)view.findViewById(R.id.imgcategory);

            cdview = (CardView) view.findViewById(R.id.card_view);
        }
    }

    public CategoryAdapter(Context mContext, List<category> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //FetchImages();

        //imageserver=imageList;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/proximanovaregular.ttf");
        holder.name.setTypeface(tf);

        final category album = albumList.get(position);
        holder.name.setText(album.getChildCategoryDescription());

        Glide.with(mContext)
                .load(Config.CATEGORYIMAGE+album.getImage())
                .into(holder.image);

        holder.cdview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferencesmodule = mContext.getSharedPreferences("MyCategory", 0);
                SharedPreferences.Editor editor = sharedPreferencesmodule.edit();
                editor.putString("category_id",album.getChildcategoryid());
                editor.putString("title",album.getChildCategoryDescription());
                editor.putString("image",album.getImage());
                editor.commit();

                Intent i=new Intent(mContext,SubcategoryActivity.class);
                mContext.startActivity(i);
//                ((Activity)mContext).finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

}