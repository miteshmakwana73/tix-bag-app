package com.jenya.tix_bag_new.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jenya.tix_bag_new.R;
import com.jenya.tix_bag_new.activity.EventTicketActivity;
import com.jenya.tix_bag_new.activity.PerformarEventActivity;
import com.jenya.tix_bag_new.jsonurl.Config;
import com.jenya.tix_bag_new.model.category;
import com.jenya.tix_bag_new.model.featuredperformer;
import com.jenya.tix_bag_new.model.slider;
import com.jenya.tix_bag_new.model.subcategory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Mitesh Makwana on 09/05/16.
 */

public class SliderAdapter extends PagerAdapter {
    private Context mContext;
    private List<slider> sliderList;

    public SliderAdapter(Context context, List<slider> sliderList) {
        this.mContext = context;
        this.sliderList = sliderList;
    }

    @Override
    public int getCount() {
        return sliderList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.raw_item_slider, null);

        final slider slider = sliderList.get(position);

        ImageView img = (ImageView) view.findViewById(R.id.img);
        final TextView name = (TextView) view.findViewById(R.id.tvtitle);
        final TextView performer_id = (TextView) view.findViewById(R.id.tvpid);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

        Glide.with(mContext)
                .load(Config.PERFORMERSIMAGE+slider.getImage())
                .into(img);

        name.setText(slider.getTitle());
        performer_id.setText(""+slider.getID());

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id= Integer.parseInt(performer_id.getText().toString());
                String nam= name.getText().toString();
//                Toast.makeText(getActivity(), name.getText().toString(), Toast.LENGTH_SHORT).show();
                SharedPreferences sharedPreferencesmodule = mContext.getSharedPreferences("MyFile", 0);
                SharedPreferences.Editor editor = sharedPreferencesmodule.edit();
                editor.putInt("performer_id", id);
                editor.putString("name",nam);
                editor.putString("parent_category_id","");
                editor.putString("image",slider.getTitle());
                editor.commit();

                Intent i=new Intent(mContext,PerformarEventActivity.class);
                mContext.startActivity(i);
            }
        });


        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}