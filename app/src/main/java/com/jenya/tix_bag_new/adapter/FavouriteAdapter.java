package com.jenya.tix_bag_new.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jenya.tix_bag_new.R;

import com.bumptech.glide.Glide;
import com.jenya.tix_bag_new.activity.PerformarEventActivity;
import com.jenya.tix_bag_new.db.Mysql;
import com.jenya.tix_bag_new.jsonurl.Config;
import com.jenya.tix_bag_new.model.favourite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mitesh Makwana on 07/05/18.
 */
public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.MyViewHolder> {

    private Context mContext;
    private List<favourite> albumList;

    Typeface tf;

    Mysql mySql;
    Cursor cursor;
    ArrayList<Integer> arryfavperformarid ;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        ImageView image,favourite;
        CardView cdview;

        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.txtperformarname);
            image=(ImageView)view.findViewById(R.id.imgperformarbg);
            favourite=(ImageView)view.findViewById(R.id.imgfavourite);
            cdview = (CardView) view.findViewById(R.id.card_view);
        }
    }

    public FavouriteAdapter(Context mContext, List<favourite> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_favourite, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //FetchImages();

        //imageserver=imageList;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/proximanovaregular.ttf");
        holder.name.setTypeface(tf);

        final favourite album = albumList.get(position);
        holder.name.setText(album.getPerformer_name());

        arryfavperformarid = new ArrayList<Integer>();

        mySql = new Mysql(mContext);
        cursor =mySql.getAllFavourites();

        if (cursor.moveToFirst()) {
            do {
                //Log.e("Quote id",cursor.getString(cursor.getColumnIndex("Quote_id")));
                arryfavperformarid.add(cursor.getInt(cursor.getColumnIndex("id")));

            } while (cursor.moveToNext());
        }

        if(arryfavperformarid.contains(album.getId()))
        {
            holder.favourite.setBackgroundResource(R.drawable.heart);
        }
        else
        {
            holder.favourite.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
        }

        Log.e("image",album.getPerformer_image());
        Glide.with(mContext)
                .load(Config.PERFORMERSIMAGE+album.getPerformer_image())
                .into(holder.image);

        holder.cdview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferencesmodule = mContext.getSharedPreferences("MyFile", 0);
                SharedPreferences.Editor editor = sharedPreferencesmodule.edit();
                editor.putInt("performer_id",album.getId());
                editor.putString("name",album.getPerformer_name());
                editor.putString("parent_category_id",album.getParent_category_id());
                editor.putString("image",album.getPerformer_image());
                editor.commit();

                Intent i=new Intent(mContext,PerformarEventActivity.class);
                mContext.startActivity(i);
//                ((Activity)mContext).finish();
            }
        });

        holder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.favourite.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
                mySql.deleteARow(String.valueOf(album.getId()));
//                Toast.makeText(mContext, "Removed from favourites", Toast.LENGTH_SHORT).show();
                holder.cdview.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

}