package com.jenya.tix_bag_new.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jenya.tix_bag_new.R;

import com.jenya.tix_bag_new.activity.EventTicketActivity;
import com.jenya.tix_bag_new.model.performerevent;
import com.jenya.tix_bag_new.model.searchevent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Mitesh Makwana on 08/05/18.
 */
public class PerformarEventAdapter extends RecyclerView.Adapter<PerformarEventAdapter.MyViewHolder> {

    private Context mContext;
    private List<performerevent> albumList;

    Typeface tf;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,detail,date,day,month;
        CardView cdview;
        LinearLayout main,lnview;

        public MyViewHolder(View view) {
            super(view);

            name = (TextView)view.findViewById(R.id.txtperformarname);
            day = (TextView)view.findViewById(R.id.txteventday);
            date = (TextView)view.findViewById(R.id.txteventdate);
//            month = (TextView)view.findViewById(R.id.txteventmonth);
            detail = (TextView)view.findViewById(R.id.txteventdetail);

//            cdview = (CardView) view.findViewById(R.id.card_view);
            main = (LinearLayout) view.findViewById(R.id.lnmain);
            lnview = (LinearLayout) view.findViewById(R.id.lnview);

        }
    }

    public PerformarEventAdapter(Context mContext, List<performerevent> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_performar_event, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //FetchImages();

        //imageserver=imageList;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/proximanovaregular.ttf");
        holder.name.setTypeface(tf);
        holder.detail.setTypeface(tf);
//        holder.month.setTypeface(tf);
        holder.date.setTypeface(tf);
        holder.day.setTypeface(tf);

        if(position==albumList.size()-1)
        {
            holder.lnview.setVisibility(View.GONE);
        }
        else
        {
            holder.lnview.setVisibility(View.VISIBLE);
        }
        final performerevent album = albumList.get(position);
        holder.name.setText(album.getName()+" at "+album.getVenue());
//        holder.event.setText(album.getVenue());


        String[] array = album.getDate().split("T");
        String dat=array[0];
        String tim=array[1];

        String newdate=dat;

        String timeformat = tim.substring(0, Math.min(tim.length(), 5));

//        holder.time.setText(timeformat);

        Date date = null;
        String day="";
        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("MMM dd");
            String inputDateStr="2013-06-24";
            date = inputFormat.parse(dat);
            newdate = outputFormat.format(date);
            DateFormat format2=new SimpleDateFormat("EEEE");
            day=format2.format(date);
            day = day.substring(0, Math.min(tim.length(), 3));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        String[] arrayn = newdate.split(" ");
        String monn=arrayn[0];
        String datn=arrayn[1];
        holder.date.setText(newdate);
        holder.day.setText(day);
//        holder.month.setText(monn);

        holder.detail.setText(timeformat +" | "+album.getCity()+" | "+album.getStateProvince());

        holder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(mContext,EventTicketActivity.class);
                i.putExtra("event_id",album.getID());
                i.putExtra("name",album.getName());
                mContext.startActivity(i);
//                ((Activity)mContext).finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

}