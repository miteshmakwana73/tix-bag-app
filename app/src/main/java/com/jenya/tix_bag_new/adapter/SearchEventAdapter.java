package com.jenya.tix_bag_new.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jenya.tix_bag_new.R;

import com.jenya.tix_bag_new.activity.EventTicketActivity;
import com.jenya.tix_bag_new.model.searchevent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by mitesh on 08/05/18.
 */

public class SearchEventAdapter extends RecyclerView.Adapter<SearchEventAdapter.MyViewHolder> {
    private Context mContext;
    private List<searchevent> albumList;
    private List<searchevent> albumListFiltered;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,detail,date,day,month;
        ImageView image;
        CardView cdview;

        LinearLayout main,lnview;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView)view.findViewById(R.id.txtperformarname);
            day = (TextView)view.findViewById(R.id.txteventday);
            date = (TextView)view.findViewById(R.id.txteventdate);
//            month = (TextView)view.findViewById(R.id.txteventmonth);
            detail = (TextView)view.findViewById(R.id.txteventdetail);
            image=(ImageView)view.findViewById(R.id.imgperformarbg);


            main = (LinearLayout) view.findViewById(R.id.lnmain);
            lnview = (LinearLayout) view.findViewById(R.id.lnview);

//            cdview = (CardView) view.findViewById(R.id.card_view);
        }
    }

    public SearchEventAdapter(Context context, List<searchevent> contactList) {
        this.mContext = context;
        this.albumList = contactList;
        this.albumListFiltered = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_search_event, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/proximanovaregular.ttf");
        holder.name.setTypeface(tf);
        holder.detail.setTypeface(tf);
//        holder.month.setTypeface(tf);
        holder.date.setTypeface(tf);
        holder.day.setTypeface(tf);

        if(position==albumList.size()-1)
        {
            holder.lnview.setVisibility(View.GONE);
        }
        else
        {
            holder.lnview.setVisibility(View.VISIBLE);
        }
        final searchevent album = albumList.get(position);
        holder.name.setText(album.getName()+" at "+album.getVenue());
//        holder.event.setText(album.getVenue());


        String[] array = album.getDate().split("T");
        String dat=array[0];
        String tim=array[1];

        String newdate=dat;

        String timeformat = tim.substring(0, Math.min(tim.length(), 5));

//        holder.time.setText(timeformat);

        Date date = null;
        String day="";
        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("MMM dd");
            String inputDateStr="2013-06-24";
            date = inputFormat.parse(dat);
            newdate = outputFormat.format(date);
            DateFormat format2=new SimpleDateFormat("EEEE");
            day=format2.format(date);
            day = day.substring(0, Math.min(tim.length(), 3));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        String[] arrayn = newdate.split(" ");
        String monn=arrayn[0];
        String datn=arrayn[1];
        holder.date.setText(newdate);
        holder.day.setText(day);
//        holder.month.setText(monn);

        holder.detail.setText(timeformat +" | "+album.getCity()+" | "+album.getStateProvince());

        /*String  input = album.getDate();
        input = input.replace("T", "\n");

        holder.time.setText(input);*/
//        holder.time.setText(album.getDate());

        /*Glide.with(mContext)
                .load(Config.PERFORMERSIMAGE+album.get())
                .into(holder.image);*/

        holder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(mContext,EventTicketActivity.class);
                i.putExtra("event_id",album.getID());
                i.putExtra("name",album.getName());
                mContext.startActivity(i);
//                ((Activity)mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumListFiltered.size();
    }

}
